# KubernetesCustomization

## ZSH_PROFILE

- Preparation
Set alias for kubecluster script in your .zshrc environment file. For example, using 'kube':
```
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
zstyle :compinstall filename $HOME/.zshrc

autoload -Uz compinit
compinit

alias kube="source $HOME/kubecluster"
```


- Usage:
```
user@antares:~% kube cluster1
Can not k8s_get current cluster environment.
>> Changing from N/A to cluster1 Kubernetes cluster.
user@antares:~[K8S:cluster1:default]% 
```

