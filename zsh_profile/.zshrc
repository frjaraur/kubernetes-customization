# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=10000
SAVEHIST=10000
zstyle :compinstall filename $HOME/.zshrc

autoload -Uz compinit
compinit

# End/Home/Delete Keys mapping
bindkey  "^[[H"   beginning-of-line
bindkey  "^[[F"   end-of-line
bindkey  "^[[3~"  delete-char

alias kube="source $HOME/kubecluster"
